# Copyright (c) 2023 Dominic Adam Walters
FROM centos:7
SHELL ["/bin/bash", "-c"]
WORKDIR /
# Update
RUN yum -y update
# Locale
RUN localedef -c -i en_US -f UTF-8 en_US.UTF-8
RUN echo "LANG=en_US.UTF-8" > /etc/locale.conf
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
RUN locale  # for visual check by user if desired
# Install git
RUN yum install -y git
