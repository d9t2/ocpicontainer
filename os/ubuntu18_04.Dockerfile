# Copyright (c) 2023 Dominic Adam Walters
FROM ubuntu:18.04
SHELL ["/bin/bash", "-c"]
WORKDIR /
# Update
RUN apt update && apt upgrade -y
# Locale
RUN apt install -y locales
RUN locale-gen en_US en_US.UTF-8
RUN update-locale LANG=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
RUN locale  # for visual check by user if desired
# Install git
RUN apt install -y git
# Pre-install tzdata to avoid interactive configuration
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/UTC
RUN apt install -y tzdata
