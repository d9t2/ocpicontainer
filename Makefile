# Copyright (c) 2023 Dominic Adam Walters
AT ?= @

from_xilinx_dir ?= /opt/Xilinx
to_xilinx_dir ?= /opt/Xilinx

operating_systems ?= $(sort $(shell \
	find . -mindepth 2 -maxdepth 2 -name "*.Dockerfile" -exec bash -c "basename {} | sed 's/\.Dockerfile//g'" \;))
opencpi_repo ?= https://gitlab.com/opencpi/opencpi.git
opencpi_branches ?= release-2.4.3 release-2.4.4 release-2.5.0
xilinx_versions ?= $(sort $(shell \
	find $(from_xilinx_dir)/Vivado -mindepth 1 -maxdepth 1 -exec bash -c "basename {}" \;))

default_os ?= centos7
default_opencpi ?= $(lastword $(opencpi_branches))
default_xilinx ?= $(lastword $(xilinx_versions))
newest ?= opencpi-$(default_opencpi)-xilinx-$(default_xilinx)-testbias

.PHONY: default
default: build-$(default_os)-$(newest)

.PHONY: all-newest
all-newest: $(addsuffix -$(newest),$(addprefix build-,$(operating_systems)))

# $(1): OS name
define generate-os-minimal

.PHONY: build-$(1)-minimal
build-$(1)-minimal:
	$(AT)echo "Creating minimal $(1) image..."
	$(AT)podman build \
		 --format docker \
		-t $(1)-minimal \
		-f os/$(1).Dockerfile \
		.
	$(AT)echo ""

.PHONY: open-$(1)-minimal
open-$(1)-minimal: build-$(1)-minimal
	$(AT)echo "Opening minimal $(1) image..."
	$(AT)podman container run \
		--rm \
		--name $(1)-minimal \
		--mount type=bind,source=$(from_xilinx_dir),target=$(to_xilinx_dir),readonly \
		-it \
		$(1)-minimal:latest

endef

$(foreach _os,$(operating_systems),\
	$(eval $(call generate-os-minimal,$(_os))))

# $(1): OS name
# $(2): OpenCPI repo branch name
define generate-os-base

.PHONY: build-$(1)-opencpi-$(2)-base
build-$(1)-opencpi-$(2)-base: build-$(1)-minimal
	$(AT)echo "Creating $(1) image with OpenCPI $(2) runtime installed..."
	$(AT)podman build \
		 --format docker \
		 -t $(1)-opencpi-$(2)-base \
		 -f install.Dockerfile \
		 --build-arg image=$(1)-minimal \
		 --build-arg repo=$(opencpi_repo) \
		 --build-arg branch=$(2) \
		 .
	$(AT)echo ""

.PHONY: open-$(1)-opencpi-$(2)-base
open-$(1)-opencpi-$(2)-base: build-$(1)-opencpi-$(2)-base
	$(AT)echo "Opening $(1) image with OpenCPI $(2) runtime installed..."
	$(AT)podman container run \
		--rm \
		--name $(1)-opencpi-$(2)-base \
		--mount type=bind,source=$(from_xilinx_dir),target=$(to_xilinx_dir),readonly \
		-it \
		$(1)-opencpi-$(2)-base:latest

endef

$(foreach _os,$(operating_systems),\
	$(foreach _branch,$(opencpi_branches),\
		$(eval $(call generate-os-base,$(_os),$(_branch)))))

# $(1): OS name
# $(2): OpenCPI repo branch name
# $(3): Xilinx version
define generate-os-xilinx-testbias

.PHONY: build-$(1)-opencpi-$(2)-xilinx-$(3)-testbias
build-$(1)-opencpi-$(2)-xilinx-$(3)-testbias: build-$(1)-opencpi-$(2)-base
	$(AT)echo "Creating $(1) image with OpenCPI $(2) testbias built for xsim $(3)..."
	$(AT)podman build \
		 --format docker \
		 -t $(1)-opencpi-$(2)-xilinx-$(3)-testbias \
		 -f build.Dockerfile \
		 --volume $(from_xilinx_dir):$(to_xilinx_dir):ro \
		 --build-arg image=$(1)-opencpi-$(2)-base \
		 --build-arg xilinx_version=$(3) \
		 .
	$(AT)echo ""

.PHONY: open-$(1)-opencpi-$(2)-xilinx-$(3)-testbias
open-$(1)-opencpi-$(2)-xilinx-$(3)-testbias: build-$(1)-opencpi-$(2)-xilinx-$(3)-testbias
	$(AT)echo "Opening $(1) image with OpenCPI $(2) testbias built for xsim $(3)..."
	$(AT)podman container run \
		--rm \
		--name $(1)-opencpi-$(2)-xilinx-$(3)-testbias \
		--mount type=bind,source=$(from_xilinx_dir),target=$(to_xilinx_dir),readonly \
		-it \
		$(1)-opencpi-$(2)-xilinx-$(3)-testbias:latest

endef

$(foreach _os,$(operating_systems),\
	$(foreach _branch,$(opencpi_branches),\
		$(foreach _xilinx,$(xilinx_versions),\
			$(eval $(call generate-os-xilinx-testbias,$(_os),$(_branch),$(_xilinx))))))

.PHONY: help
help:
	$(AT)echo "Usage:"
	$(AT)echo "  Functional Images:"
	$(AT)echo "    Build:"
	$(AT)for string in $$(./make_utils.py --get-working); do \
		echo "      build-$$string"; \
	done
	$(AT)echo "    Open:"
	$(AT)for string in $$(./make_utils.py --get-working); do \
		echo "      open-$$string"; \
	done

.PHONY: help-all
help-all:
	$(AT)echo "Usage:"
	$(AT)echo ""
	$(AT)echo "  Images:"
	$(AT)echo "    Build:"
	$(AT)for os in $(operating_systems); do \
		echo "      $$os"; \
		echo "        make build-$$os-minimal"; \
		for branch in $(opencpi_branches); do \
			echo "        make build-$$os-opencpi-$$branch-base"; \
			for xilinx in $(xilinx_versions); do \
				echo "        make build-$$os-opencpi-$$branch-xilinx-$$xilinx-testbias"; \
			done; \
		done; \
		echo ""; \
	done
	$(AT)echo ""
	$(AT)echo "    Open:"
	$(AT)for os in $(operating_systems); do \
		echo "      $$os"; \
		echo "        make open-$$os-minimal"; \
		for branch in $(opencpi_branches); do \
			echo "        make open-$$os-opencpi-$$branch-base"; \
			for xilinx in $(xilinx_versions); do \
				echo "        make open-$$os-opencpi-$$branch-xilinx-$$xilinx-testbias"; \
			done; \
		done; \
		echo ""; \
	done
