# ocpicontainer

## Dependencies

For Ubuntu 20.10 and newer:
```bash
sudo apt-get -y install make podman
```

For Arch Linux:
```bash
sudo pacman --noconfirm -S make podman
```

For CentOS 7 (requires "Extras" repo):
```bash
sudo yum -y install make podman
```

## Quickstart

Do you know what OS you want a container for?

Do you know what branch you want?

Do you know what Xilinx Vivado version you want (and have installed at
`/opt/Xilinx`)?

Then, to get a container with `testbias` built:
```bash
make build-<os>-opencpi-<branch>-xilinx-<version>-testbias
```

## General Usage

Run `make help` to get a list of all of the images you can build and open.

This list will be dependent on the versions of the Xilinx Vivado toolchain
that you have installed at `/opt/Xilinx`.

Image names are of the forms:

* `<os>-minimal`

  * The simplest version of the specified operating system that can clone
    OpenCPI and execute its install script successfully.

* `<os>-opencpi-<branch>-base`

  * The operating system with the OpenCPI runtime built.

* `<os>-opencpi-<branch>-xilinx-<version>-testbias`

  * The operating system with the hdl assembly `testbias` built for the `xsim`
    platform.

Valid make rules are an image name with the prefix:

* `build-<image>`

  * Build the image. This will build all dependent images.

* `open-<image>`

  * Open an image. This will build the image if necessary.

## Copyright

Copyright (c) 2023 Dominic Adam Walters
