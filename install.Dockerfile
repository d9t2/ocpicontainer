# Copyright (c) 2023 Dominic Adam Walters
ARG image
FROM $image
SHELL ["/bin/bash", "-c"]
WORKDIR /
# Clone the OpenCPI repo
ARG repo
ARG branch
RUN \
  git clone $repo && \
  cd opencpi && \
  git checkout $branch
# Install OpenCPI
WORKDIR /opencpi
RUN ./scripts/install-opencpi.sh --minimal
