#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters

import argparse
import json
import sys


def format_triplet(triplet: dict):
    """Return the name of a build based on the given triplet.

    Args:
        triplet (dct): The triplet to format. Must contain the keys `os`,
        `branch`, and `xilinx`.
    """
    return f"{triplet['os']}-opencpi-{triplet['branch']}-xilinx-{triplet['xilinx']}-testbias"  # noqa


if __name__ == "__main__":
    # Start argument parser
    parser = argparse.ArgumentParser(description="Make utils for `ocpidocker`")
    parser.add_argument(
        "--get-working",
        action="store_true",
        default=False)
    parser.add_argument(
        "--get-not-working",
        action="store_true",
        default=False)
    args, unknown = parser.parse_known_args()
    if len(unknown) > 0:
        print(f"Unknown arguments: {unknown}")
        sys.exit(1)
    # Actions
    if args.get_working:
        with open("status.json", "rt") as f:
            working = json.loads(f.read())["working"]
        for rule in [format_triplet(triplet) for triplet in working]:
            print(rule)
    if args.get_not_working:
        with open("status.json", "rt") as f:
            not_working = json.loads(f.read())["notworking"]
        for rule in [format_triplet(triplet) for triplet in not_working]:
            print(rule)
