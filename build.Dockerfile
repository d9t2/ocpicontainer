# Copyright (c) 2023 Dominic Adam Walters
ARG image
FROM $image
SHELL ["/bin/bash", "-c"]
WORKDIR /opencpi
# Select Xilinx version
ARG xilinx_version
ENV OCPI_XILINX_VERSION=$xilinx_version
# Build minimal primitives in `ocpi.core`
RUN \
  source ./cdk/opencpi-setup.sh -r && \
  ocpidev build \
  -d projects/core/hdl/primitives \
  --hdl-platform xsim
# Build `hdl/platforms/xsim` in `ocpi.core`
RUN \
  source ./cdk/opencpi-setup.sh -r && \
  ocpidev build \
  -d projects/core/hdl/platforms/xsim \
  --hdl-platform xsim \
  --workers-as-needed
# Build `hdl/assemblies/testbias` in `ocpi.assets`
RUN \
  source ./cdk/opencpi-setup.sh -r && \
  ocpidev build \
  -d projects/assets/hdl/assemblies/testbias \
  --hdl-platform xsim \
  --workers-as-needed
